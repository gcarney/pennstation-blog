Blog module for PennStation
===========================

This gem adds blogging functionality to a PennStation website.

Requirements
------------

- [PennStation Core 4.0.0][pennstation_core]

Installation
------------

Place this in the website's Gemfile:

```ruby
gem "penn_station_blog", "=1.0.0", git: "ssh://git@bitbucket.org/gcarney/pennstation-blog.git", tag: "v1.0.0"
```

Add this to config/routes.rb *after* the line "PennStation::Engine, :at => 'admin'"

```ruby
mount PennStationBlog::Engine, :at => "admin/blog"
```

And run:

    $ bundle install
    $ bundle exec rails g penn_station_blog:bootstrap


How to run tests
----------------

Coming soon.

[pennstation_core]: https://bitbucket.org/gcarney/pennstation-core
