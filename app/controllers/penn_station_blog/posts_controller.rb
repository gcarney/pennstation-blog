require_dependency "penn_station_blog/application_controller"

module PennStationBlog
  class PostsController < ApplicationController
    before_action :set_post, only: [:show, :edit, :update, :destroy]

#    track_admin_activity

    def index
      @posts = Post.all
    end

    def show
    end

    def new
      @post = Post.new
    end

    def edit
    end

    def create
      @post = Post.new(post_params)
      update_categories

      if @post.save
        redirect_to edit_post_path(@post), notice: "Successfully created #{@post.title}."
      else
        render action: 'new'
      end
    end

    def update
      update_categories
      if @post.update(post_params)
        redirect_to posts_path
      else
        render action: 'edit'
      end
    end

    def destroy
      @post.destroy
      redirect_to posts_path, notice: "post #{@post.title} was successfully deleted."
    end

    private

    def set_post
      @post = Post.find(params[:id])
    end

    def post_params
      params.require(:post).permit(:title, :summary, :body, :publication_date, :publish, :credit)
    end

    def update_categories
      categories = params['post']['category_ids'].reject{|i| i == ""} || []
      @post.categories = categories.map{|c| PennStationBlog::Category.find(c)}.compact
    end
  end
end
