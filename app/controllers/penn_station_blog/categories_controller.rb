require_dependency "penn_station_blog/application_controller"

module PennStationBlog
  class CategoriesController < ApplicationController
    before_action :set_category, only: [:show, :edit, :update, :destroy]

#    track_admin_activity

    def index
      @categories = Category.all
    end

    def show
    end

    def new
      @category = Category.new
    end

    def edit
    end

    def create
      @category = Category.new(category_params)

      if @category.save
        redirect_to categories_path, notice: "Successfully created #{@category.name}."
      else
        render action: 'new'
      end
    end

    def update
      if @category.update(category_params)
        redirect_to categories_path
      else
        render action: 'edit'
      end
    end

    def destroy
      @category.destroy
      redirect_to categories_path, notice: "category #{@category.name} was successfully deleted."
    end

    private

    def set_category
      @category = Category.find(params[:id])
    end

    def category_params
      params.require(:category).permit(:name)
    end
  end
end
