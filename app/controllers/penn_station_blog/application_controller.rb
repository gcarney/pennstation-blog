module PennStationBlog
  class ApplicationController < ::PennStation::ApplicationController
    layout 'layouts/penn_station/application'
  end
end
