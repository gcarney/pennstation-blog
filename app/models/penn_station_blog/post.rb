module PennStationBlog
  class Post < ActiveRecord::Base
    has_and_belongs_to_many :categories, class_name: 'PennStationBlog::Category'

    validates_presence_of :title
    validates_uniqueness_of :title, :scope => :publication_date

    before_save :make_permalink_title

       # used for admin activities summary

    def name
      title
    end

    def permalink_path()
      "/blog/" + permalink_title
    end

    private

    def make_permalink_title
      date_url = publication_date.to_s.gsub("-", "/")
      self.permalink_title = date_url + "/" + urlize(title)
    end

    # same as for Page
    def urlize(s)
      (s || "").gsub(/ /, "_").gsub(/[^\d\w\-_]/, "").downcase
    end
  end
end
