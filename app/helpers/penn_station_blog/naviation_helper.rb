module PennStationBlog
  module NavigationHelper
    def blog_location(page_type='Blog')
      (p=PennStation::Page.find_by_ptype(page_type)).nil? ? '#' : p.url
    end
  end
end
