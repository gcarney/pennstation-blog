module PennStationBlog
  module Generators
    class BootstrapGenerator < Rails::Generators::Base
      source_root File.expand_path("../templates", __FILE__)
      desc "Bootstrap PennStation Blog in a new client project"

      def copy_skeleton
        %w( app db ).each do |dir|
          directory dir, dir
        end
      end

      def run_all
        puts "\nInstalling migrations"
        puts `rake penn_station_blog:install:migrations`

        puts "\nMigrating database"
        puts `rake db:migrate`

        puts "\nSeeding database"
        puts `rake db:seed`
      end

      def complete
        puts "\nPennStation Blog install complete!"
      end
    end
  end
end
