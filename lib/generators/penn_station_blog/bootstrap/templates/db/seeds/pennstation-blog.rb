puts "\nSeeding Blog"

p = PennStation::Page.where(url: '/blog').first_or_create do |page|
  page.title = 'Blog'
  puts "  #{page.title}"
  page.ptype = 'Blog'
end
p.published = true
p.save

[ 'Blog Category 1', 'Blog Category 2', 'Blog Category 3' ].each do |category|
  PennStationBlog::Category.where(name: category).first_or_create
end
