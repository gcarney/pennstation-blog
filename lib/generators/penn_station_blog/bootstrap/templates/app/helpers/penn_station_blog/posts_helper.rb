module PennStationBlog
  module PostsHelper
    def gather_posts
      posts = sidebar_posts = PennStationBlog::Post.where(publish: true).order('publication_date desc')

      if params.has_key?(:post_id)
        posts = [ PennStationBlog::Post.find(params[:post_id]) ]
      elsif params.has_key?(:category_id)
        posts = PennStationBlog::Category.find(params[:category_id]).posts.where(publish: true).order('publication_date desc')
      end

      [ posts, sidebar_posts ]
    end
  end
end
