module PennStationBlog
  module PageHelper
    def blog_location(page_type='Blog')
      (p=PennStation::Page.find_by_ptype(page_type)).nil? ? '#' : p.url
    end
  end
end
