# see this page for engines with rspec, factory_girl and capybara
# http://viget.com/extend/rails-engine-testing-with-rspec-capybara-and-factorygirl

$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "penn_station_blog/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "penn_station_blog"
  s.version     = PennStationBlog::VERSION
  s.authors     = ["warren vosper"]
  s.email       = ["straydogsw@gmail.com"]
  s.homepage    = "http:://cctsbaltimore.org"
  s.summary     = "PennStation blog."
  s.description = "PennStation blog as a rails engine."

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["spec/**/*"]

  s.add_dependency "rails", "~> 4.2.1"
  s.add_dependency "jquery-rails"
end
