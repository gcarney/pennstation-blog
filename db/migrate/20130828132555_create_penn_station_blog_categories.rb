class CreatePennStationBlogCategories < ActiveRecord::Migration
  def change
    create_table :penn_station_blog_categories do |t|
      t.string :name

      t.timestamps
    end
  end
end
