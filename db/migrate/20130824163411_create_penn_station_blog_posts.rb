class CreatePennStationBlogPosts < ActiveRecord::Migration
  def change
    create_table :penn_station_blog_posts do |t|
      t.string      :title
      t.string      :permalink_title
      t.text        :summary
      t.text        :body
      t.string      :credit
      t.boolean     :publish, :default => false
      t.date        :publication_date

      t.timestamps
    end
  end
end
