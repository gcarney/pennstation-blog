class CreateBlogCategoriesBlogPosts < ActiveRecord::Migration
  def change
    create_table :penn_station_blog_categories_posts, :id => false do |t|
      t.references :category
      t.references :post
    end
  end
end
