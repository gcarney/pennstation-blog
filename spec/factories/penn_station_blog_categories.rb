# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :penn_station_blog_category, :class => 'PennStationBlog::Category' do
    name "MyString"
  end
end
