# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :penn_station_blog_post, :class => 'PennStationBlog::Post' do
    title "MyString"
    permalink_title "MyString"
    summary "summary"
    body "body"
    publish false
    credit "An Author"
    publication_date { Time.now }
  end
end
