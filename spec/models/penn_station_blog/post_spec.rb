require 'spec_helper'

module PennStationBlog
  describe Post do
    let(:post) { FactoryGirl.build(:penn_station_blog_post) }

    context "when a post is valid" do

      it "should have a title" do
        post.should respond_to(:title)
      end

      it "should have a permalink_title" do
        post.should respond_to(:permalink_title)
      end

      it "should have a summary" do
        post.should respond_to(:summary)
      end

      it "should have a summary" do
        post.should respond_to(:summary)
      end

      it "should have a body" do
        post.should respond_to(:body)
      end

      it "should have a publish flag" do
        post.should respond_to(:publish)
      end

      it "should have a credit" do
        post.should respond_to(:credit)
      end

      it "should be valid" do
        post.should be_valid
      end
    end

    context "when posts are not valid" do

      it "should fail when the title is missing" do
        post = FactoryGirl.build(:penn_station_blog_post, title: nil)
        post.should_not be_valid
      end

      it "should fail when the title is a duplicate" do
        post.save
        post2 = FactoryGirl.build(:penn_station_blog_post)
        post2.should_not be_valid
      end
    end

    context "when posts have categories" do
      let(:category) { FactoryGirl.build(:penn_station_blog_category, name: 'cat1') }

      it "should remember them" do
        post.categories << category
        post.save
        post.reload
        post.categories.count.should == 1
      end
    end
  end
end
