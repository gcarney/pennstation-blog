require 'spec_helper'

module PennStationBlog
  describe Category do
    let(:category) { FactoryGirl.build(:penn_station_blog_category) }

    context "when a category is valid" do

      it "should have a name" do
        category.should respond_to(:name)
      end

      it "should be valid" do
        category.should be_valid
      end
    end

    context "when categorys are not valid" do

      it "should fail when the name is missing" do
        category = FactoryGirl.build(:penn_station_blog_category, name: nil)
        category.should_not be_valid
      end

      it "should fail when the name is a duplicate" do
        category.save
        category2 = FactoryGirl.build(:penn_station_blog_category)
        category2.should_not be_valid
      end
    end
  end
end
